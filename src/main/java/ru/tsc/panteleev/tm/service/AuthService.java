package ru.tsc.panteleev.tm.service;

import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.api.service.IUserService;
import ru.tsc.panteleev.tm.exception.field.LoginEmptyException;
import ru.tsc.panteleev.tm.exception.field.PasswordEmptyException;
import ru.tsc.panteleev.tm.exception.system.AccessDeniedException;
import ru.tsc.panteleev.tm.exception.system.LoginOrPasswordIncorrectException;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new LoginOrPasswordIncorrectException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new LoginOrPasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        return userService.findById(getUserId());
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }
}
