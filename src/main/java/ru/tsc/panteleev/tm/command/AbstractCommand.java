package ru.tsc.panteleev.tm.command;

import ru.tsc.panteleev.tm.api.model.ICommand;
import ru.tsc.panteleev.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        if (name != null && !name.isEmpty()) result += "Command '" + name + "'";
        if (argument != null && !argument.isEmpty()) result += ", argument '" + argument + "'";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
