package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.User;
import java.util.List;

public interface IUserRepository {

    User create(String login, String password);

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

}
