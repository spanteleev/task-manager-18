package ru.tsc.panteleev.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
