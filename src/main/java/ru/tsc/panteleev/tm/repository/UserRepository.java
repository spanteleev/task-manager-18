package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public User create(String login, String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return  user;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findById(final String id) {
        for (User user : users) {
            if (id.equals(user.getId()))
                return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : users) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : users) {
            if (email.equals(user.getEmail()))
                return user;
        }
        return null;
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

}
